package Controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import Models.Alunos;
import Models.Turma;
import Services.DAO;

/**
 * Servlet implementation class controler
 */
@WebServlet(urlPatterns = { "/main", "/turmas", "/alunos", "/adicionar", "/select", "/update", "/delete" })
public class controler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAO dao = new DAO();
	Turma turma = new Turma();
	Alunos aluno = new Alunos();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public controler() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getServletPath();

		if (action.equals("/main")) {
			ListarTurmas(request, response);

		} else if (action.equals("/alunos")) {
			ListarAlunos(request, response);

		} else if (action.equals("/select")) {
			selecionarAluno(request, response);

		} else if (action.equals("/update")) {
			editarAluno(request, response);

		} else if (action.equals("/adicionar")) {
			cadastrarAlunos(request, response);

		} else if (action.equals("/delete")) {
			excluirAluno(request, response);

		} else {
			response.sendRedirect("Index.html");
		}
	}

	// listar as turmas
	protected void ListarTurmas(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ArrayList<Turma> lista = dao.listarTurmas();
		request.setAttribute("turma", lista);

		RequestDispatcher rd = request.getRequestDispatcher("turmas.jsp");
		rd.forward(request, response);
		response.sendRedirect("main");
	}

	// listar os alunos
	protected void ListarAlunos(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String param = request.getParameter("idturma");
		System.out.println(param);
		aluno.setIdturma(request.getParameter("idturma"));
		ArrayList<Alunos> lista = dao.ListarAlunos(aluno);
		request.setAttribute("alunos", lista);

		RequestDispatcher rd = request.getRequestDispatcher("alunos.jsp");
		rd.forward(request, response);
		response.sendRedirect("alunos");

	}

	// cadastrar alunos
	protected void cadastrarAlunos(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		aluno.setNome(request.getParameter("nome"));
		aluno.setIdade(request.getParameter("idade"));
		aluno.setIdturma(request.getParameter("idturma"));

		
		dao.adicionarAluno(aluno);
		response.sendRedirect("main");

	}

	// selecionar aluno
	protected void selecionarAluno(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	
		
		aluno.setIdaluno(request.getParameter("idaluno"));
		
		dao.selecionarAluno(aluno);
		
		
		request.setAttribute("idaluno", aluno.getIdaluno());
		request.setAttribute("nome", aluno.getNome());
		request.setAttribute("idade", aluno.getIdade());
		request.setAttribute("idturma", aluno.getIdturma());

		RequestDispatcher rd = request.getRequestDispatcher("editar.jsp");
		rd.forward(request, response);

	}

	
	
	
	// editar aluno
	protected void editarAluno(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		aluno.setIdaluno(request.getParameter("idaluno"));
		aluno.setNome(request.getParameter("nome"));
		aluno.setIdade(request.getParameter("idade"));
		aluno.setIdturma(request.getParameter("idturma"));

		dao.editarAluno(aluno);
		response.sendRedirect("main");

	}
	
	//deletar Aluno
	protected void excluirAluno(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String id= request.getParameter("idaluno");
		
		aluno.setIdaluno(request.getParameter("idaluno"));
		System.out.println(id);
		
		dao.deletarAluno(aluno);
		response.sendRedirect("main");

	}

}

package Models;

// TODO: Auto-generated Javadoc
/**
 * The Class Turma.
 */
public class Turma {
	
	/** The idturma. */
	private String idturma;
	
	/** The nome. */
	private String nome;
	
	
	/**
	 * Instantiates a new turma.
	 */
	public Turma() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Instantiates a new turma.
	 *
	 * @param idturma the idturma
	 * @param nome the nome
	 */
	public Turma(String idturma, String nome) {
		super();
		this.idturma = idturma;
		this.nome = nome;
	}

	/**
	 * Gets the idturma.
	 *
	 * @return the idturma
	 */
	public String getIdturma() {
		return idturma;
	}
	
	/**
	 * Sets the idturma.
	 *
	 * @param idturma the new idturma
	 */
	public void setIdturma(String idturma) {
		this.idturma = idturma;
	}
	
	/**
	 * Gets the nome.
	 *
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Sets the nome.
	 *
	 * @param nome the new nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	

}

package Services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import Models.Alunos;
import Models.Turma;

// TODO: Auto-generated Javadoc
/**
 * The Class DAO.
 */
public class DAO {

	/** The driver. */
	private String driver = "com.mysql.cj.jdbc.Driver";

	/** The url. */
	private String url = "jdbc:mysql://localhost:3306/escola?useTimezone=true&serverTimezone=UTC";

	/** The user. */
	private String user = "root";

	/** The password. */
	private String password = "kuma1234";

	/**
	 * Conectar.
	 *
	 * @return the connection
	 */
	private Connection conectar() {
		Connection con = null;
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, user, password);
			return con;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	/**
	 * Listar turmas.
	 *
	 * @return the array list
	 */
	public ArrayList<Turma> listarTurmas() {
		String read = "select * from turma order by nome";
		ArrayList<Turma> turmas = new ArrayList<>();
		try {
			// abri conexao
			Connection con = conectar();
			PreparedStatement pst = con.prepareStatement(read);
			ResultSet rs = pst.executeQuery();

			// o la�o abaixo ser� execultado enquanto houver contatos
			while (rs.next()) {
				// variaveis de apoio
				String idturma = rs.getString(1);
				String nome = rs.getString(2);
				// populando o arrayList
				turmas.add(new Turma(idturma, nome));
			}
			// encerrar a conex�o
			con.close();
			return turmas;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}

	}

	/**
	 * Listar alunos.
	 *
	 * @param aluno the aluno
	 * @return the array list
	 */
	public ArrayList<Alunos> ListarAlunos(Alunos aluno) {
		String read = "select * from alunos where idturma=? order by nome";
		ArrayList<Alunos> alunos = new ArrayList<>();
		try {
			// abri conexao
			Connection con = conectar();
			PreparedStatement pst = con.prepareStatement(read);
			pst.setString(1, aluno.getIdturma());
			ResultSet rs = pst.executeQuery();

			// o la�o abaixo ser� execultado enquanto houver contatos
			while (rs.next()) {
				// variaveis de apoio
				String idaluno = rs.getString(1);
				String nome = rs.getString(2);
				String idade = rs.getString(3);
				String idturma = rs.getString(3);
				// populando o arrayList
				alunos.add(new Alunos(idaluno, nome, idade, idturma));
			}
			// encerrar a conex�o
			con.close();
			return alunos;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}

	}

	/**
	 * Adicionar aluno.
	 *
	 * @param aluno the aluno
	 */
	public void adicionarAluno(Alunos aluno) {
		String create = "insert Into alunos (nome,idade,idturma) values (?,?,?)";

		try {
			// abri conexao
			Connection con = conectar();

			// preparar a query para execu�ao no banco de dados
			PreparedStatement pst = con.prepareStatement(create);
			pst.setString(1, aluno.getNome());
			pst.setString(2, aluno.getIdade());
			pst.setString(3, aluno.getIdturma());

			// execultar a query
			pst.executeUpdate();

			// encerrar a conex�o
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Selecionar aluno.
	 *
	 * @param aluno the aluno
	 */
	// crud select
	public void selecionarAluno(Alunos aluno) {
		String read = "select * from alunos where idaluno=?";
		try {
			Connection con = conectar();
			PreparedStatement pst = con.prepareStatement(read);
			pst.setString(1, aluno.getIdaluno());
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {

				aluno.setIdaluno(rs.getString(1));
				aluno.setNome(rs.getString(2));
				aluno.setIdade(rs.getString(3));
				aluno.setIdturma(rs.getString(4));
			}

			con.close();

		} catch (Exception e) {
			System.out.println(e);

		}

	}

	/**
	 * Editar aluno.
	 *
	 * @param aluno the aluno
	 */
	public void editarAluno(Alunos aluno) {
		String update = "update alunos set nome=?, idade=?, idturma=? where idaluno=?";
		try {
			Connection con = conectar();
			PreparedStatement pst = con.prepareStatement(update);

			pst.setString(1, aluno.getNome());
			pst.setString(2, aluno.getIdade());
			pst.setString(3, aluno.getIdturma());
			pst.setString(4, aluno.getIdaluno());
			pst.executeUpdate();

			con.close();

		} catch (Exception e) {
			System.out.println(e);

		}

	}

	/**
	 * Deletar aluno.
	 *
	 * @param aluno the aluno
	 */
	public void deletarAluno(Alunos aluno) {

		String delete = "delete from alunos where idaluno=?";
		try {
			Connection con = conectar();
			PreparedStatement pst = con.prepareStatement(delete);

			pst.setString(1, aluno.getIdaluno());
			pst.executeUpdate();

			con.close();

		} catch (Exception e) {
			System.out.println(e);

		}

	}

////	//Teste De Conex�o
//	
//	public void testeConexao() {
//		try {
//			Connection con = conectar();
//			System.out.println(con);
//			
//		} catch (Exception e) {
//			System.out.println(e);
//		}
//	}
}
